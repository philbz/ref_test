#!/usr/bin/env bash

git for-each-ref --format="ref=%(refname)" refs/heads | \
while read entry 
do
  echo "$entry"
done
